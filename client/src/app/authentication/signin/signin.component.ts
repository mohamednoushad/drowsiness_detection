import { AuthenticationService } from './../services/authentication.service';
import { User, Provider } from './../services/user';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  user: User = <User>new Object();
  otp: number = null;
  showOtpform: boolean = false;
  providers: Array<Provider>;
  provider: Provider;
  constructor(private router: Router,
    private authService: AuthenticationService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService) {

  }

  ngOnInit() {
  }


  generateOtp(): void {
    this.user.isRegistered = true;
    console.log(this.user);
    this.authService.generateOTP(this.user)
      .subscribe(response => {
        if (response) {
          this.authService.getRegisteredProviders(this.user)
            .subscribe(providers => {
              if (providers) {
                this.provider = providers[0];
                this.providers = providers;
                console.log(this.providers);
                this.showOtpform = true;
              }
              this.toastr.success("Success!!!", "OTP generated successfully");
            })
        }
      },
        error => {
          console.error('Oops:', error);
          this.toastr.error("OTP generation failed", "Please make sure you have entered all the data correctly");
        });
  }


  setuserProfile(value: any) {
    this.authService.userProfile=value;
  }


  signin(): void {
    this.spinner.show();
    if(this.user.user === "user") {
      this.setuserProfile("user");
      this.authService.userType.next("user");
    }
    else if(this.user.user === "police") {
      this.setuserProfile("police");
      this.authService.userType.next("police");
    }
    else if(this.user.user === "insurance") {
      this.setuserProfile("insurance");
      this.authService.userType.next("insurance");
    }
    else if(this.user.user === "rta") {
      this.setuserProfile("rta");
      this.authService.userType.next("rta");
    }

    this.authService.loggedIn.next(true);
    this.spinner.hide();
    this.router.navigate(['/home']);
  }

}
