import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { NavbarService } from '../shared/sevices/navbar/navbar.service';

@Component({
  selector: "app-authentication",
  templateUrl: "./authentication.component.html",
  styleUrls: ["./authentication.component.scss"]
})
export class AuthenticationComponent implements OnInit {
  constructor(private router: Router, private nav: NavbarService) {}

  ngOnInit() {
    //this.nav.hide();
  }
  gotToHome() {
    this.router.navigate(["/home"]);
  }
}
