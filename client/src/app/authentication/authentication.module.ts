import { FormsModule } from '@angular/forms';
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthenticationComponent } from "./authentication.component";
import { AngularMaterialModule } from "./../angular-material/angular-material.module";
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { RouterModule } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
@NgModule({
  declarations: [AuthenticationComponent, SigninComponent, SignupComponent],
  imports: [CommonModule, AngularMaterialModule, RouterModule, FormsModule],
  providers: [AuthenticationService],
})
export class AuthenticationModule {}
