import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  getIncidents(): Observable<any> {
    return this.http.get(`http://115.114.36.146:4400/driver/incidents`)
      .pipe(map(response => {
        console.log(response);
        return response
        
      }));
  }

  getUsers() : Observable<any> {
    return this.http.get(`http://115.114.36.146:4400/accessGivenAddresses`)
      .pipe(map(response => {
        console.log(response);
        return response
        
      }));
  }

  getInsurers(address) : Observable<any> {
    return this.http.get(`http://115.114.36.146:4400/getInsurerDetails`)
      .pipe(map(response => {
        if(response && response){
          return response;
        }
        
      }));
  }


  revokeDelegation(data): Observable<any> {
    console.log("revoke", data);
    return this.http.post(`http://115.114.36.146:4400/insurance/revoke`, data)
      .pipe(map(response => response));
  }


  delegate(data): Observable<any> {
    return this.http.post(`http://115.114.36.146:4400/insurance/add`, data)
      .pipe(map(response => response));
  }

  getIncidentLog() : Observable<any> {
    return this.http.get(`http://115.114.36.146:4400/police/driver/incidents`)
      .pipe(map(response => {
        return response;
      }));
  }

  getTrackingInfo() : Observable<any> {
    let policeLog = this.http.get(`http://115.114.36.146:4400/auditLogOfPoliceAccess`);
    let insuranceLog = this.http.get(`http://115.114.36.146:4400/auditLogOfInsuranceAccess`);
    return forkJoin([policeLog, insuranceLog]);
  }

  
















  getFile(data): Observable<any> {
    return this.http.post(`http://115.114.36.146:4445/api/user/file`, data)
      .pipe(map(response => {
        return response;
      }));
  }

  getVisits(data, provider): any {
    const params = new HttpParams().set('identifier', data).set('provider', provider);
    return this.http.get(`http://115.114.36.146:4445/api/user/visits`, { params })
      .pipe(map(response => {
        return response;
      }));
  }

  getObservations(data, provider): any {
    const params = new HttpParams().set('subject', data).set('provider', provider);
    return this.http.get(`http://115.114.36.146:4445/api/user/observations`, { params })
      .pipe(map(response => {
        return response;
      }));
  }

  getPrescriptions(personId, provider): any {
    const params = new HttpParams().set('personId', personId).set('provider', provider);
    return this.http.get(`http://115.114.36.146:4445/prescriptions`, { params })
      .pipe(map(response => {
        return response;
      }));
  }

  getNominees(data): Observable<any> {
    const params = new HttpParams().set('userId', data);
    return this.http.get(`http://115.114.36.146:4445/api/delegate/nominees`, { params })
      .pipe(map(response => response));
  }

  // delegate(data): Observable<any> {
  //   return this.http.post(`http://115.114.36.146:4445/api/delegate/add`, data)
  //     .pipe(map(response => response));
  // }

  // revokeDelegation(data): Observable<any> {
  //   const httpOptions = {
  //     headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: data
  //   };
  //   console.log("revoke", data);
  //   return this.http.delete(`http://115.114.36.146:4445/api/delegate/revoke`, httpOptions)
  //     .pipe(map(response => response));
  // }


  // getGrantor(data: any): Observable<any> {
  //   const params = new HttpParams().set('userId', data);
  //   return this.http.get(`http://115.114.36.146:4445/api/delegate/nominators`, { params })
  //     .pipe(map(response => response));
  // }


  // revokeDelegation(data): Observable<any> {
  //   const httpOptions = {
  //     headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: data
  //   };
  //   console.log("revoke", data);
  //   return this.http.delete(`http://115.114.36.146:4445/api/delegate/revoke`, httpOptions)
  //     .pipe(map(response => response));
  // }

  grantAccess(data): Observable<any> {
    return this.http.post(`http://115.114.36.146:4445/api/delegate/grantAccess`, data)
      .pipe(map(response => response));
  }

  getAllocatedUsers(ssn, user): Observable<any> {
    const params = new HttpParams().set('userId', ssn).set('user', user);
    return this.http.get(`http://115.114.36.146:4445/api/user/allocated`, { params })
      .pipe(map(response => {
        return response;
      }));
  }

  manageAccess(data): Observable<any> {
    return this.http.post(`http://115.114.36.146:4445/api/ownership`, data)
      .pipe(map(response => {
        return response;
      }));
  }

  getProvidersOfUser(data) : Observable<any> {
    console.log("calling get providers api from front end");
    console.log(data);
    let queryParams='';
    if(data.ssn){
      queryParams = `?userId=${data.ssn}`;
    }
    else if(data.address){
      queryParams = `?address=${data.address}`;
    }
 
    return this.http.get(`http://115.114.36.146:4445/api/user/providerList`+ queryParams)
    .pipe(map(response => {
      return response;
    }));
  }

  getDemographics(grantor, recipient, provider): Observable<any> {
    const params = new HttpParams().set('recipient', recipient).set('grantor', grantor).set('providerNo',provider)
    console.log(params);
    return this.http.get(`http://115.114.36.146:4445/api/user/grantor`, { params })
    .pipe(map(response => {
      return response;
    }));
  }

  getABCData(): Observable<any> {
    return this.http.get(`http://115.114.36.146:4400/driver/incidents`)
      .pipe(map(response => {
        if(response && response){
          return response;
        }
        
      }));
  }


}
