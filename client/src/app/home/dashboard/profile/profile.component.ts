import { HomeService } from './../../home.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  profileInfo: any;
  demographics: any;
  fileURL: string = "";
  constructor(private authService: AuthenticationService, private userService: HomeService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    var profileInfo = this.getUserProfile();
    this.profileInfo= profileInfo;
    // let user = profileInfo ? profileInfo.creds: null;
    // const data = {
    //   ssn: user ? user.ssn : null,
    //   account: profileInfo? profileInfo.user.account: null,
    //   user: user ? user.user: null,
    //   provider: user ? user.emrDetails.number: null
    // };
    // this.profileInfo = profileInfo;
    // if (this.profileInfo && this.profileInfo.clinicalData && this.profileInfo.clinicalData.body.entry[0])
    //     this.demographics = this.profileInfo.clinicalData.body.entry[0].resource;
    // if(user && user.user !== "doctor"){
    //   this.userService.getFile(data).subscribe(response => {
    //     if(response && response.data) {
    //       this.fileURL = response.data;
    //     }
    //   });
    // }

  }

  getUserProfile() {
    return this.authService.userProfile;
  }



}
