import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientComponent } from './patient.component';
import { AngularMaterialModule } from 'src/app/angular-material/angular-material.module';

@NgModule({
  declarations: [PatientComponent],
  imports: [
    CommonModule, AngularMaterialModule
  ],
  exports: [
    PatientComponent
  ]
})
export class PatientModule { }
