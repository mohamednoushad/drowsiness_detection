import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { NavbarService } from '../shared/sevices/navbar/navbar.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  role: string
  user: any;
  constructor(private router: Router, private route: ActivatedRoute, private authService: AuthenticationService) {

   }

  ngOnInit() {
    this.user = this.authService.userProfile;
    if(!this.user){
      this.router.navigate(['/']);
    }
    //get user type here and if user type is patient -- go to patient dashboard or doctor dashboard
  }

}
