import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-content-management',
  templateUrl: './content-management.component.html',
  styleUrls: ['./content-management.component.scss']
})
export class ContentManagementComponent implements OnInit {

  @Input() user;
  route: string;
  constructor(private router: Router) { 
    console.log(this.router.url)
    this.route = this.router.url;
  }
      
  ngOnInit() {
    console.log(this.user);
  }

}
