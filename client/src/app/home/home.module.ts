import { DashboardModule } from './dashboard/dashboard.module';
import { AngularMaterialModule } from './../angular-material/angular-material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { AppSideNavComponent } from './app-side-nav/app-side-nav.component'; 
import { HomeRoutingModule } from './home-routing.module';
@NgModule({
  declarations: [HomeComponent, /*AppSideNavComponent*/],
  imports: [
    CommonModule, AngularMaterialModule, RouterModule, DashboardModule, HomeRoutingModule
  ]
})
export class HomeModule { }
