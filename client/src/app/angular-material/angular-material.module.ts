import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import {
  MatExpansionModule,
  MatButtonModule,
  MatSidenavModule,
  MatToolbarModule,
  MatGridListModule,
  MatFormFieldModule,
  MatInputModule,
  MatDialogModule,
  MatNativeDateModule,
  MatTableModule,
  MatMenuModule
} from "@angular/material";
import { MatDatepickerModule } from "@angular/material/datepicker";
// import { MatRadioModule } from "@angular/material/radio";
import { MatSelectModule } from "@angular/material/select";
// import { MatSliderModule } from "@angular/material/slider";
// import { MatDividerModule } from "@angular/material/divider";
import { MatListModule } from "@angular/material/list";
//import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


@NgModule({
  imports: [
    CommonModule,
    MatSidenavModule,
    MatExpansionModule,
    MatButtonModule,
    MatToolbarModule,
    MatGridListModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    MatTableModule,
    MatMenuModule
  ],
  exports: [
    MatSidenavModule,
    MatExpansionModule,
    MatButtonModule,
    MatToolbarModule,
    MatGridListModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    MatTableModule,
    MatMenuModule
  ]
})
export class AngularMaterialModule {}
