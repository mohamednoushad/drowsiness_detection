
###Client : 

This is where the angular app runs
To start the client run 'npm run start' from the client directory
This starts the app by calling ngserve -h 192.168.2.27 -p 4300 
We will be able to view it at 192.168.2.27:4300 within Attinad network and for public access with http://115.114.36.146:4300

#Point to note if running locally, change 
#Search for 4300 and change it accordingly to the total address where you run the server (ip:port);
#On line number 174, change configuration to where the http-server is running eg: window.open("http://localhost:9090","_blank);
In the server deployment, we have hosted it on 4600 and hence it is "http://115.114.36.146:4600";

#Server

cd into server directory
run 'node server.js' or 'pm2 start server.js' depending on your use case

#http-server

This brings up the html page showing the drowsiness calculator

cd into http-server and then into ai directory
from there , run 'http-server -p 4600'
which will bring up the server on port 4600.

#
If you are facing any other issues, ensure you have angular-cli and http-server installed globally.
This can be run by running npm install -g angular-cli and npm install -g http-server
node v 9 s preferred








